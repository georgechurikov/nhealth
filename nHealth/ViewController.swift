//
//  ViewController.swift
//  nHealth
//
//  Created by admin on 26.01.2021.
//

import UIKit

class ViewController: UIViewController {
    var registerButton = UIButton()
    var isNotEmtyText = false
    var confirmButton = UIButton()
    let clouseKeyboard = UIPanGestureRecognizer()
    let newPasswordButton = UIButton()
    let code = UILabel()
    var endButton = UIButton()
    var codeView = UIView()
    var segmentSex = UISegmentedControl()
    let labelDateBirthdey = UILabel()
    var loginEnterTaxtField = UITextField()
    var passwordEnterTaxtField = UITextField()
    var nameTaxtField = UITextField()
    var surnameEnterTaxtField = UITextField()
    var patronymicRegisterTextField = UITextField()
    var loginRegisterTextField = UITextField()
    var passwordRegisterTextField = UITextField()
    let myScrollView = UIScrollView()
    let underView = UIView()
    let labelTitleEnter = UILabel()
    var enterButton = UIButton()
    var defaultHeight:CGFloat = 0
    var saveDataButton = UIButton()
    var picker = UIDatePicker()
    var codeTextField = UITextField()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated:true)
        createDefaultHeight()
        createScrollView()
        createLoginEnterTextField()
        createTitleImage()
        createPasswordEnterTextField()
        creteEnterButton()
        createRegisterButton()
        createLabelTitleEnter()
        createSaveDataButton()
        overwriteAccountButton()
        keyboardFrame()
    }
    func createDefaultHeight() {
        defaultHeight = self.view.frame.height / 15
    }
    @objc func actionSaveData(param:UIButton) {
        UserDefaults.standard.set(!UserDefaults.standard.bool(forKey: "saveData"), forKey: "saveData")
        createSaveDataButton()
    }
    func overwriteAccountButton() {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Відновити", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.italicSystemFont(ofSize: 23)
        button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        button.layer.cornerRadius = defaultHeight / 3
        button.layer.masksToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        myScrollView.addSubview(button)
        button.widthAnchor.constraint(equalToConstant: self.view.frame.width / 3.4).isActive = true
        button.bottomAnchor.constraint(equalTo: myScrollView.centerYAnchor, constant: (UIScreen.main.bounds.height / 2 - defaultHeight)).isActive = true
        button.centerXAnchor.constraint(equalTo: self.myScrollView.centerXAnchor,constant: self.view.frame.width * 0.33 ).isActive = true
        button.heightAnchor.constraint(equalToConstant: defaultHeight ).isActive = true
    }
    func createSaveDataButton() {
        var image = UIImage()
        if UserDefaults.standard.bool(forKey: "saveData") {
            image = UIImage(systemName: "checkmark.square.fill")!
        } else {
            image = UIImage(systemName: "checkmark.square")!
        }
        
        saveDataButton.setImage(image, for: .normal)
        saveDataButton.semanticContentAttribute = .playback
        saveDataButton.tintColor = .black
        saveDataButton.setTitle("Запам'ятати", for: .normal)

        saveDataButton.translatesAutoresizingMaskIntoConstraints = false
        saveDataButton.titleLabel?.font = UIFont.italicSystemFont(ofSize: 17)
        saveDataButton.setTitleColor(.black, for: .normal)
        saveDataButton.addTarget(self, action: #selector(actionSaveData(param:)), for: .touchUpInside)
        self.myScrollView.addSubview(saveDataButton)
        saveDataButton.topAnchor.constraint(equalTo: passwordEnterTaxtField.bottomAnchor, constant: 10).isActive = true
        saveDataButton.leadingAnchor.constraint(equalTo: self.myScrollView.leadingAnchor, constant: 20).isActive = true
        saveDataButton.heightAnchor.constraint(equalToConstant: defaultHeight / 2).isActive = true
        saveDataButton.trailingAnchor.constraint(equalTo: self.myScrollView.centerXAnchor).isActive = true
    }
    func createLabelTitleEnter() {
        labelTitleEnter.text = " Введіть данні для входу"
        labelTitleEnter.font = UIFont.italicSystemFont(ofSize: 15)
        labelTitleEnter.textAlignment = .left
        labelTitleEnter.textColor = #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1)
        labelTitleEnter.translatesAutoresizingMaskIntoConstraints = false
        self.myScrollView.addSubview(labelTitleEnter)
        labelTitleEnter.bottomAnchor.constraint(equalTo: loginEnterTaxtField.topAnchor, constant: -5).isActive = true
        labelTitleEnter.leadingAnchor.constraint(equalTo: self.myScrollView.leadingAnchor, constant: 20).isActive = true
    }
    func createRegisterButton() {
        createButtons(button: &registerButton, title: " Aккаунт")
        registerButton.layer.cornerRadius = defaultHeight  / 3
        registerButton.backgroundColor = #colorLiteral(red: 0.8362033042, green: 0, blue: 0, alpha: 1)
        registerButton.tintColor = .white
        registerButton.setImage(UIImage(systemName: "plus"), for: .normal)
        self.myScrollView.addSubview(registerButton)
        registerButton.widthAnchor.constraint(equalToConstant: self.view.frame.width / 3.4).isActive = true
        registerButton.bottomAnchor.constraint(equalTo: myScrollView.centerYAnchor, constant: (UIScreen.main.bounds.height / 2 - defaultHeight)).isActive = true
        registerButton.centerXAnchor.constraint(equalTo: self.myScrollView.centerXAnchor,constant: -self.view.frame.width * 0.33).isActive = true
        registerButton.heightAnchor.constraint(equalToConstant: defaultHeight ).isActive = true
    }
    func createLoginEnterTextField() {
        createTextFields(textField: &loginEnterTaxtField, placeholder: "Введіть логін", imageName: "person.crop.square.fill")
        loginEnterTaxtField.bottomAnchor.constraint(equalTo: myScrollView.centerYAnchor, constant: 0).isActive = true
    }
    func createPasswordEnterTextField() {
        createTextFields(textField: &passwordEnterTaxtField, placeholder: "Введіть пароль", imageName: "lock.square.fill")
        passwordEnterTaxtField.topAnchor.constraint(equalTo: loginEnterTaxtField.bottomAnchor, constant: defaultHeight / 2).isActive = true
    }
    func createTitleImage() {
        let titleImage = UIImageView()
        guard let image = UIImage(named: "TitleHealth-removebg-preview") else { return }
        titleImage.image = image
        titleImage.sizeToFit()
        titleImage.translatesAutoresizingMaskIntoConstraints = false
        self.myScrollView.addSubview(titleImage)
        titleImage.centerYAnchor.constraint(equalTo: self.myScrollView.centerYAnchor, constant: -self.view.frame.height * 0.25).isActive = true
        titleImage.heightAnchor.constraint(equalToConstant: defaultHeight * 1.5 ).isActive = true
        titleImage.widthAnchor.constraint(equalToConstant: defaultHeight * 7.5 ).isActive = true
        titleImage.centerXAnchor.constraint(equalTo: myScrollView.centerXAnchor).isActive = true
    }
    func creteEnterButton()
    {
        createButtons(button: &enterButton, title: "Увійти")
        enterButton.layer.cornerRadius = defaultHeight  / 3
        self.myScrollView.addSubview(enterButton)
        enterButton.widthAnchor.constraint(equalToConstant: self.view.frame.width / 3.4).isActive = true
        enterButton.bottomAnchor.constraint(equalTo: myScrollView.centerYAnchor, constant: (UIScreen.main.bounds.height / 2 - defaultHeight)).isActive = true
        enterButton.centerXAnchor.constraint(equalTo: self.myScrollView.centerXAnchor).isActive = true
        enterButton.heightAnchor.constraint(equalToConstant: defaultHeight ).isActive = true
    }
    
    
    func createTextFields(textField:inout UITextField,placeholder:String,imageName:String) {
        guard let image =  UIImage(systemName: imageName) else {
            return
        }
        let imageView = UIImageView()
        imageView.image = image
        imageView.frame = CGRect(x: 10, y: defaultHeight / 4, width: defaultHeight / 2, height: defaultHeight / 2)
        imageView.tintColor = #colorLiteral(red: 0.1215686277, green: 0.01176470611, blue: 0.4235294163, alpha: 1)
        textField.attributedPlaceholder = NSAttributedString(string: "My Placeholder", attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1)])
        textField.layer.shadowRadius = 3
        textField.layer.shadowOffset = CGSize(width: 0, height: 2)
        textField.layer.shadowColor = #colorLiteral(red: 0.001304504663, green: 0.003943181947, blue: 0.1537871653, alpha: 0.7997158632)
        textField.layer.shadowOpacity = 5
        textField.delegate = self
        textField.addSubview(imageView)
        textField.textAlignment = .center
//        textField.backgroundColor = #colorLiteral(red: 0.01409911762, green: 0.7093242827, blue: 0.9686274529, alpha: 0.2745454296)
        textField.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

        textField.layer.cornerRadius = defaultHeight / 3
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.layer.borderWidth = 2
        textField.layer.borderColor =  #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 0.1761994015)
        textField.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        textField.font = UIFont.italicSystemFont(ofSize: 23)
        textField.placeholder = placeholder
        textField.layer.masksToBounds = false
        myScrollView.addSubview(textField)
        textField.heightAnchor.constraint(equalToConstant: defaultHeight).isActive = true

        if self.view.frame.width < 500 {
            textField.leadingAnchor.constraint(equalTo: self.myScrollView.leadingAnchor, constant: 20).isActive = true
            textField.trailingAnchor.constraint(equalTo: self.myScrollView.leadingAnchor, constant: self.view.frame.width - 20).isActive = true
        } else {
            textField.widthAnchor.constraint(equalToConstant: 500).isActive = true
            textField.centerXAnchor.constraint(equalTo: self.myScrollView.centerXAnchor).isActive = true
        }
        
    }
    
    func createButtons(button:inout UIButton,title:String) {
        button.setTitle(title, for: .normal)
        button.layer.shadowRadius = 3.0
        button.layer.shadowOffset = CGSize(width: 2, height: 2)
        button.layer.shadowColor = #colorLiteral(red: 0.1215686277, green: 0.01176470611, blue: 0.4235294163, alpha: 1)
        button.layer.shadowOpacity = 4.0
        button.titleLabel?.font = UIFont.italicSystemFont(ofSize: 23)
        button.backgroundColor = #colorLiteral(red: 0.4666328924, green: 0.5718969814, blue: 1, alpha: 0.5)
        button.backgroundColor = #colorLiteral(red: 0.1288301943, green: 0.1594161728, blue: 0.2809446701, alpha: 0.5757483678)
        button.layer.borderWidth = 2
        button.layer.borderColor = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 0.2421042335)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius =  defaultHeight /  2
        button.layer.masksToBounds = true
        button.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .highlighted)
    }
    
   
    func createScrollView() {
        myScrollView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        myScrollView.alpha = 1
        myScrollView.isScrollEnabled = false
        myScrollView.translatesAutoresizingMaskIntoConstraints = false
        myScrollView.contentSize = CGSize(width: UIScreen.main.bounds.width * 3, height: UIScreen.main.bounds.height * 3)
        myScrollView.isPagingEnabled = true
        self.view.addSubview(myScrollView)
        myScrollView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        myScrollView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        myScrollView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        myScrollView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    }
    func keyboardFrame() {
        
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil) { (a) in
            self.view.frame.origin.y = -150
        }
        
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil) { (a) in
            self.view.frame.origin.y = 0
        }
    }
}

extension ViewController : UISearchTextFieldDelegate , UITextFieldDelegate , UIGestureRecognizerDelegate  {
    func createNewView() {
        
    }
    
   
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
          return true
      }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.5) {
            textField.backgroundColor = #colorLiteral(red: 0.4666328924, green: 0.5718969814, blue: 1, alpha: 0.5551666866)
        }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.5) {
            textField.backgroundColor = .white
        }
    }
    
    }
